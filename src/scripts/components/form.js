export class Form {
  constructor() {
    this.list = [];
    this.files = [];

    this.selectors();
    this.events();
  }

  selectors() {
    this.input = document.querySelector("#displayImage");
    this.previewContainer = document.querySelector(".tourist-spot-img"); // div a qual irá servir de container para a imagem!
    this.previewText = this.previewContainer.querySelector("#insert-image-text"); // texto no centro da div!
    this.previewImage = this.previewContainer.querySelector(".preview-image"); // tag img que deverá receber o src da imagem!
    //
    this.title = document.querySelector("#itemTitle");
    this.description = document.querySelector("#itemDescription");
    //
    this.button = document.querySelector("#submitData");
    this.content = document.querySelector(".cards");
  }

  events() {
    this.input.addEventListener("change", this.displayImage.bind(this));
    this.button.addEventListener("click", this.touristPoint.bind(this));
  }

  // A função abaixo é responsável pela seleção e exibição da imagem
  displayImage() {
    const preview = this.previewImage;
    const file = this.input.files[0];
    const reader = new FileReader();

    reader.addEventListener(
      "load",
      function () {
        preview.src = reader.result;
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);

      this.previewText.style.display = "none";
      this.previewImage.style.display = "block";
    }

    console.log(file);
  }

  touristPoint(event) {
    event.preventDefault();

    const placeImage = this.previewImage.src;
    const placeName = this.title.value;
    const placeDescription = this.description.value;

    if (placeName !== "") {
      const place = {
        image: placeImage,
        title: placeName,
        description: placeDescription,
      };

      this.list.push(place);
      this.renderListItems();
      this.resetInputs();
    }
  }

  renderListItems() {
    let itemsStructure = "";

    this.list.forEach(function (place) {
      itemsStructure += `
            <li class="tourist-point">
                <div class="tourist-point-data">
                    <span class="image-wrapper">
                        <figure>
                            <img src="${place.image}" alt="${place.title}" title="${place.title}"/>
                        </figure>
                    </span>

                    <div class="bottom-card-wrapper">
                      <strong class="card-title">${place.title}</strong>

                      <span class="text-wrapper">
                          <span class="description-wrapper">
                            <p class=""card-desc>${place.description}</p>
                          </span>
                      </span>
                    </div>
                </div>
            </li>
        `;
    });

    this.content.innerHTML = itemsStructure;
  }

  resetInputs(event) {
    this.title.value = "";
    this.description.value = "";

    const imageShowing = (this.previewImage.style.display = "block");

    const resetImageField =
      imageShowing == (this.previewImage.style.display = "block")
        ? (this.previewImage.style.display = "none") && (this.previewText.style.display = "block")
        : (this.previewImage.style.display = "block") && (this.previewText.style.display = "none");
  }
}
