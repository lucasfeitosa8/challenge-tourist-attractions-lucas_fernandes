export class Validation {
  constructor() {
    this.selectors();
    this.events();
  }

  selectors() {
    this.title = document.querySelector("#itemTitle").value;
    this.description = document.querySelector("#itemDescription").value;
    this.button = document.querySelector("#submitData");
    this.body = document.getElementsByTagName("body");
  }

  events() {
    this.button.addEventListener("click", this.validateFields.bind(this));
  }

  validateFields() {
    // const btn = (this.button.disabled = true);
    // if (this.title && this.description == "") {
    //   this.btn;
    //   alert("Testando");
    // }
    // if (this.title && this.description != "") {
    //   this.btn.disabled = false;
    // }
  }
}
