import { Form } from "./components/form";
import { Validation } from "./components/validation";

document.addEventListener("DOMContentLoaded", function () {
  new Form();
  new Validation();
});
